﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SysINFO
{
	public partial class MainFormSysINFO : Form
	{
		Thread buttonUpdThread;
		public Thread startThread;
		public Thread stopThread;
		public MainFormSysINFO()
		{
			startThread = new Thread(StartThread);
			stopThread = new Thread(StopThread);
			buttonUpdThread = new Thread(ButtnOK);
			InitializeComponent();
		}

		private void Copy(object sender, EventArgs e)
		{
			if (!buttonUpdThread.IsAlive)
			{
				buttonUpdThread = new Thread(ButtnOK);
				buttonUpdThread.Start();
			}
			Clipboard.SetText(tbInfo.Text.Replace("\n","\r\n").Replace("\r\r\n","\r\n"));
		}
		private void ButtnOK()
		{
			string tmp = butCopy.Text;
			Invoke(new Action(() => butCopy.Text = "OK"));
			Thread.Sleep(200);
			Invoke(new Action(() => butCopy.Text = tmp));
			Thread.Sleep(200);
		}

		private void MainFormLoad(object sender, EventArgs e)
		{
			startThread.Start();
		}
		public void StartThread()
		{
			for (int i = -50; i <= 0; i++)
			{
				Invoke(new Action(() => Opacity = Math.Sqrt(1 - Math.Pow((i * 2) / 100D, 2))));
				Thread.Sleep(15);
			}
			Program.WriteTextBox(Program.AllInfo, tbInfo);
			Invoke(new Action(() => butCopy.Enabled = true));
		}
		public void StopThread()
		{
			while (startThread.IsAlive) Thread.Sleep(60);
			for (int i = 0; i >= -50; i--)
			{
				Invoke(new Action(() => Opacity = Math.Sqrt(1 - Math.Pow((i * 2) / 100D, 2))));
				Thread.Sleep(15);
			}
			Invoke(new Action(() => this.Dispose()));
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			e.Cancel = true;
			if (!stopThread.IsAlive)
			{
				stopThread = new Thread(StopThread);
				stopThread.Start();
			}
		}
	}
}
