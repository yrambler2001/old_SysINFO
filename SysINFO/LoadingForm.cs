﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SysINFO
{
	public partial class LoadingForm : Form
	{
		public Thread startThread;
		public Thread stopThread;
		public LoadingForm()
		{
			startThread = new Thread(StartThread);
			stopThread = new Thread(StopThread);
			InitializeComponent();
		}

		private void LoadingForm_Load(object sender, EventArgs e)
		{
			startThread.Start();
			pb1.Style = ProgressBarStyle.Marquee;
			pb1.MarqueeAnimationSpeed = 30;
		}
		public void StartThread()
		{
			for (int i = -50; i <= 0; i++)
			{
				Invoke(new Action(() => Opacity = Math.Sqrt(1 - Math.Pow((i * 2) / 100D, 2))));
				Thread.Sleep(15);
			}
		}
		public void StopThread()
		{
			while (startThread.IsAlive) Thread.Sleep(60);
			for (int i = 0; i >= -50; i--)
			{
				Invoke(new Action(() => Opacity = Math.Sqrt(1 - Math.Pow((i * 2) / 100D, 2))));
				Thread.Sleep(15);
			}
			Invoke(new Action(() => this.Dispose()));
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			e.Cancel = true;
			if (!stopThread.IsAlive)
			{
				stopThread = new Thread(StopThread);
				stopThread.Start();
			}
		}
	}
}
