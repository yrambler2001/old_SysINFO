﻿namespace SysINFO
{
    partial class MainFormSysINFO
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
			this.tbInfo = new System.Windows.Forms.RichTextBox();
			this.butCopy = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// tbInfo
			// 
			this.tbInfo.BackColor = System.Drawing.SystemColors.Control;
			this.tbInfo.DetectUrls = false;
			this.tbInfo.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.tbInfo.Location = new System.Drawing.Point(5, 5);
			this.tbInfo.Name = "tbInfo";
			this.tbInfo.Size = new System.Drawing.Size(400, 500);
			this.tbInfo.TabIndex = 0;
			this.tbInfo.Text = "";
			// 
			// butCopy
			// 
			this.butCopy.Enabled = false;
			this.butCopy.Location = new System.Drawing.Point(5, 510);
			this.butCopy.Name = "butCopy";
			this.butCopy.Size = new System.Drawing.Size(400, 35);
			this.butCopy.TabIndex = 1;
			this.butCopy.Text = "Скопіювати";
			this.butCopy.UseVisualStyleBackColor = true;
			this.butCopy.Click += new System.EventHandler(this.Copy);
			// 
			// MainFormSysINFO
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(410, 550);
			this.Controls.Add(this.butCopy);
			this.Controls.Add(this.tbInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "MainFormSysINFO";
			this.Opacity = 0D;
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "SysINFO";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.RichTextBox tbInfo;
        public System.Windows.Forms.Button butCopy;
    }
}

